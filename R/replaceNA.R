
# returns x if it is not NA, otherwise returns the replacement (it's like SQL's ISNULL() function)
replaceNA <- function(x, replacement = 0) {
    result <- safeIfElse(
        cond = !is.na(x),
        yes = x,
        no = replacement
    )
    return(result)
}

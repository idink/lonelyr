

# getCumProdByGroup <- function(x, by, na.rm = TRUE) {
#     if(na.rm) {
#         x <- ifelse(is.na(x), yes=1, no=x)
#     }
#
#     the.dt <- data.table::data.table(Value = x, Group = by, Order = 1:length(x))
#     result <- as.data.frame(the.dt[, list(CumProd = cumprod(Value), Order = Order), by=list(Group)])
#     result <- result[order(result$Order),]
#     return(result$CumProd)
# }


getCumProdByGroup <- function(x, by, na.rm = FALSE) {
    if(na.rm) {
        x <- ifelse(is.na(x), yes=0, no=x)
    }

    the.dt <- data.table::data.table(Value = x, Order = 1:length(x))
    group.names <- c()
    if(is.list(by)) {
        if(length(by)>1) {
            the.range <- 1:length(by)
        } else {
            the.range <- 1
        }

        for(i in the.range) {
            group.name <- paste0('Group', i)
            the.dt[, group.name] <- by[i]
            group.names <- c(group.names, group.name)
        }

        groups <- unique(as.data.frame(the.dt)[, group.names, drop = F])
        groups$Combined <- 1:nrow(groups)

        the.dt <- merge(the.dt, groups, by = group.names, all.x = TRUE)
    } else {
        the.dt$Combined <- by
    }


    the.dt <- the.dt[order(the.dt$Order),]
    result <- as.data.frame(the.dt[, list(CumProd = cumprod(Value), Order = Order), by=list(Combined)])
    result <- result[order(result$Order),]
    return(result$CumProd)
}


# returns Cumulative Sum of a Vector grouped by another Vector
getCumProdByGroup2 <- function(x, by, na.rm = TRUE) {
    the.data <- data.frame(Value = x, Group = by, Type = 'Original')
    the.data$Order <- 1:nrow(the.data)

    the.data$NonZeroValue <- ifelse(the.data$Value==0, yes=1, no=the.data$Value)
    the.data$AfterZero <- getCumSumByGroup(x = the.data$Value==0, by = the.data$Group, na.rm = T)
    the.data$AfterZero <- ifelse(the.data$AfterZero>0, yes = TRUE, no = FALSE)

    .invProd <- function(x) {
        return(1/prod(x, na.rm = TRUE))
    }
    .cumProd <- function(x) {
        x <- ifelse(is.na(x), yes=1, no=x)
        return(cumprod(x))
    }

    aggs <- aggregate(x = the.data[,c('Value','NonZeroValue')], by = list(the.data$Group), FUN=.invProd)
    colnames(aggs) <- c('Group', 'Value', 'NonZeroValue')
    aggs$Order <- nrow(the.data)+1:nrow(aggs)
    aggs$Type <- 'InverseProd'
    aggs$AfterZero <- 0

    the.data.and.invs <- rbind(the.data, aggs)
    the.data.and.invs <- the.data.and.invs[order(the.data.and.invs$Group, the.data.and.invs$Order),]


    the.data.and.invs$CumProd <- .cumProd(x = the.data.and.invs$NonZeroValue)*(the.data.and.invs$AfterZero!=TRUE)

    the.data.and.cumprod <- subset(the.data.and.invs, Type=='Original')

    the.data.and.cumprod <- the.data.and.cumprod[order(the.data.and.cumprod$Order),]
    if(!na.rm) {
        the.data.and.cumprod$CumProd <- ifelse(is.na(the.data.and.cumprod$Value), yes=NA, no=the.data.and.cumprod$CumProd)
    }
    return(the.data.and.cumprod$CumProd)
}

# # Example
# my.data <- data.frame(Car = c('Honda', 'Toyota', NA, 'Ford', 'Honda', 'Toyota', NA), TotalBought = c(100, NA, -5, 1, 200, 20, -5))
# my.data$CumProd <- getCumProdByGroup(x = my.data$TotalBought, by = my.data$Car, na.rm = FALSE)
# my.data

# Example 2 (zeros)
# library(lonelyr)
# getCumProdByGroup(x = c(1,0,1,2,3,4,5,0), by = c(1,1,1,2,2,3,3,1))
# getCumProdByGroup2(x = c(1,1,1,2,1,0,0,0), by =c(1,1,1,2,2,3,3,1))
# getCumProdByGroup(x = c(1,NA,1,2,1,0,0,0), by = 1, na.rm = T)
# getCumSumByGroup(x = c(1,NA,1,2,1,0,0,0), by = 1, na.rm = T)


# Example
# my.data <- data.frame(Car = c('Honda', 'Toyota', NA, 'Ford', 'Honda', 'Toyota', NA), TotalBought = c(100, NA, -5, 1, 200, 20, -5))
# my.data$CumProd <- getCumProdByGroup(x = my.data$TotalBought, by = my.data$Car, na.rm = F)
# my.data$CumProdNaRm <- getCumProdByGroup(x = my.data$TotalBought, by = my.data$Car, na.rm = T)
# my.data$CumProd2 <- getCumProdByGroup2(x = my.data$TotalBought, by = my.data$Car, na.rm = F)
# my.data$CumProd2NaRm <- getCumProdByGroup2(x = my.data$TotalBought, by = my.data$Car, na.rm = F)
# my.data
#
# x.city <- c(rep('Paris', times=6), rep('London', times=6))
# x.businesses <- rep(c(rep('Theatres', times = 3), rep('Banks', times = 3)), times=2)
# example2 <- data.frame(city = x.city, business = x.businesses, score = rep(c(2,2,0,2)), times = 3)
# example2$CumProd <- getCumProdByGroup(x = example2$score, by = list(example2$city, example2$business), na.rm = T)
# example2

isValidEmail <- function(x) {
    email.regex <- "\\<[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,}\\>"
    result <- grepl(pattern = email.regex, x = as.character(x), ignore.case=TRUE)
    return(result)
}

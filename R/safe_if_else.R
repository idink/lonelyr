
safeIfElse <- function(cond, yes, no){
    class.y <- class(yes)
    X <- ifelse(cond,yes,no)
    class(X) <- class.y
    return(X)
}

# capitalizes every word after a separator 'sep'
# "hello world" will become HelloWorld
capitalizeFirstLetter <- function(x, sep = ' ') {
    s <- strsplit(x = x, split = sep, fixed = TRUE)[[1]]
    result <- paste(toupper(substring(s, 1,1)), substring(s, 2),
          sep="", collapse=sep)
    return(result)
}

capitalizeFirstLetter <- Vectorize(FUN = capitalizeFirstLetter)
